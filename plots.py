import numpy as np
import matplotlib.pyplot as plt

fso = np.genfromtxt("results/scores/res-so-f.txt", delimiter=" ")
jso = np.genfromtxt("results/scores/res-so-j.txt", delimiter=" ")
fsk = np.genfromtxt("results/scores/res-sk-f.txt", delimiter=" ")
jsk = np.genfromtxt("results/scores/res-sk-j.txt", delimiter=" ")
fn = np.genfromtxt("results/scores/res-n-f.txt", delimiter=" ")
jn = np.genfromtxt("results/scores/res-n-j.txt", delimiter=" ")
multif = np.genfromtxt("results/scores/res-multi-f.txt", delimiter=" ")
multij = np.genfromtxt("results/scores/res-multi-j.txt", delimiter=" ")
pjsk = np.genfromtxt("results/scores/param-sk-j.txt", delimiter=" ")
pjso = np.genfromtxt("results/scores/param-so-j.txt", delimiter=" ")
pjn = np.genfromtxt("results/scores/param-n-j.txt", delimiter=" ")
runj = np.genfromtxt("results/scores/runtime-j.txt", delimiter=" ")
runf = np.genfromtxt("results/scores/runtime-f.txt", delimiter=" ")

fsoa = np.genfromtxt("results/scores/res-area-so-f.txt", delimiter=" ")
jsoa = np.genfromtxt("results/scores/res-area-so-j.txt", delimiter=" ")
fska = np.genfromtxt("results/scores/res-area-sk-f.txt", delimiter=" ")
jska = np.genfromtxt("results/scores/res-area-sk-j.txt", delimiter=" ")
fna = np.genfromtxt("results/scores/res-area-n-f.txt", delimiter=" ")
jna = np.genfromtxt("results/scores/res-area-n-j.txt", delimiter=" ")

fsoroc = np.genfromtxt("results/scores/roc-so-f2.txt", delimiter=" ")
jsoroc = np.genfromtxt("results/scores/roc-so-j.txt", delimiter=" ")
fskroc = np.genfromtxt("results/scores/roc-sk-f.txt", delimiter=" ")
jskroc = np.genfromtxt("results/scores/roc-sk-j.txt", delimiter=" ")
fnroc = np.genfromtxt("results/scores/roc-n-f.txt", delimiter=" ")
jnroc = np.genfromtxt("results/scores/roc-n-j.txt", delimiter=" ")

"""
x =["Dice So", "MCC So", "Dice Sk", "MCC Sk","Dice D","MCC D"]
plt.plot(x, multif, label = "Frangi")
plt.plot(x, multij, label = "Jerman")
plt.title("Multiscale Framework ")
plt.xticks(x)
plt.legend()
plt.show()

x = [1, 2, 3, 4,5,6,7,8,9,10]
plt.plot(x, fsk[0], label = "Skimage")
plt.plot(x, fso[0], label = "Sobel")
plt.plot(x, fn[0], label = "Derivative")
plt.xticks(x)
plt.title("Frangi Dice")
plt.ylabel('Dice Score')
plt.legend()
plt.show()

plt.plot(x, fsk[1], label = "Skimage")
plt.plot(x, fso[1], label = "Sobel")
plt.plot(x, fn[1], label = "Derivative")
plt.xticks(x)
plt.title("Frangi MCC ")
plt.ylabel('MCC Score')
plt.legend()
plt.show()

x = [1, 2, 3, 4,5,6,7,8,9,10]
plt.plot(x, jsk[0], label = "Skimage")
plt.plot(x, jso[0], label = "Sobel")
plt.plot(x, jn[0], label = "Derivative")
plt.title("Jerman Dice")
plt.xticks(x)
plt.ylabel('Dice Score')
plt.legend()
plt.show()

plt.plot(x, jsk[1], label = "Skimage")
plt.plot(x, jso[1], label = "Sobel")
plt.plot(x, jn[1], label = "Derivative")
plt.title("Jerman MCC ")
plt.xticks(x)
plt.ylabel('MCC Score')
plt.legend()
plt.show()


print(pjsk[0])
x = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
#x=["0.5 & 15","0.8 & 25","1.5 & 9","1 & 10"]
plt.plot(x, pjsk[0], label = "Skimage")
plt.plot(x, pjso[0], label = "Sobel")
plt.plot(x, pjn[0], label = "Derivative")
plt.title("Jerman Dice Parameters")
plt.xticks(x)
plt.ylabel('Dice Score')
plt.legend()
plt.show()

plt.plot(x, pjsk[1], label = "Skimage")
plt.plot(x, pjso[1], label = "Sobel")
plt.plot(x, pjn[1], label = "Derivative")
plt.title("Jerman MCC Parameters")
plt.xticks(x)
plt.ylabel('MCC Score')
plt.legend()
plt.show()


x = [1, 2, 3, 4,5,6,7,8,9,10]
plt.plot(x, runj[2], label = "Skimage")
plt.plot(x, runj[1], label = "Sobel")
plt.plot(x, runj[0], label = "Derivative")
plt.title("Jerman Runtime")
plt.xticks(x)
plt.ylabel('Runtime in seconds')
plt.legend()
plt.show()

plt.plot(x, runf[2], label = "Skimage")
plt.plot(x, runf[1], label = "Sobel")
plt.plot(x, runf[0], label = "Derivative")
plt.title("Frangi Runtime ")
plt.xticks(x)
plt.ylabel('Runtime in seconds')
plt.legend()
plt.show()
"""

x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
plt.plot(x, fska[0], label="Skimage")
plt.plot(x, fsoa[0], label="Sobel")
plt.plot(x, fna[0], label="Derivative")
plt.xticks(x)
plt.title("Frangi Dice Area")
plt.ylabel('Dice Score')
plt.legend()
plt.show()

plt.plot(x, fska[1], label="Skimage")
plt.plot(x, fsoa[1], label="Sobel")
plt.plot(x, fna[1], label="Derivative")
plt.xticks(x)
plt.title("Frangi MCC Area")
plt.ylabel('MCC Score')
plt.legend()
plt.show()

x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
plt.plot(x, jska[0], label="Skimage")
plt.plot(x, jsoa[0], label="Sobel")
plt.plot(x, jna[0], label="Derivative")
plt.title("Jerman Dice Area")
plt.xticks(x)
plt.ylabel('Dice Score')
plt.legend()
plt.show()

plt.plot(x, jska[1], label="Skimage")
plt.plot(x, jsoa[1], label="Sobel")
plt.plot(x, jna[1], label="Derivative")
plt.title("Jerman MCC Area")
plt.xticks(x)
plt.ylabel('MCC Score')
plt.legend()
plt.show()

"""
plt.plot(fsoroc[1], fsoroc[0], label = "Sobel Frangi")
plt.title("ROC")
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.legend()
plt.show()"""
