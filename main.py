import numpy as np
from PIL import Image
from singleFilter import computeFrangiImages, computeJermanImages
from multiscaleFramework import multiscale
from evaluation import computeConfusionMatrix, diceScore, mccScore
from utils import normalize, imagePad
import matplotlib.pyplot as plt

imaIndex = ["12", "13", "14", "15", "28", "29", "30", "31",
            "108", "109", "110", "120", "121", "122"]

plus1 = [-70, -70, -70, -50, -50, -30, -
         30, -30, -30, -210, -100, -30, -10, -30]
plus3 = [+30, +50, +150, +70, +70, +100, +
         100, +100, +30, +70, +160, +90, +20, +100]


def thresholdUnfiltered(imaIndex, plus1, plus3):
    # Computes Dice and MCC scores for unfiltered images
    arr = np.zeros((2, 1))

    for indx, i in enumerate(imaIndex):
        img = np.array(Image.open('PIMA/images/'+i+'.jpg'))
        img = normalize(img)
        res = img.copy()

        res[img > 0.5] = 0
        res[img <= 0.5] = 1

        truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
        truth = truth[:, :, 1]
        truth = normalize(truth)
        truth = imagePad(truth, res)

        truth[truth <= 0.9] = 0
        truth[truth > 0.9] = 1

        idx0 = np.nonzero(truth.ravel() == 1)[0]
        idxs = [idx0.min(), idx0.max()]
        out = np.column_stack(np.unravel_index(idxs, truth.shape)).flatten()

        truth = truth[out[0]-20:out[2]+20,
                      out[1]+plus1[indx]:out[3]+plus3[indx]]
        res = res[out[0]-20:out[2]+20, out[1]+plus1[indx]:out[3]+plus3[indx]]

        mat = computeConfusionMatrix(
            truth.astype(np.int32), res.astype(np.int32))
        arr[0] += diceScore(mat)
        arr[1] += mccScore(mat)

    arr = arr/len(imaIndex)
    with open("res-no-filter.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))


def filterImages(imaIndex):
    # Filters images with different standard deviations σ and saves filered images
    for i in imaIndex:
        img = np.array(Image.open('PIMA/images/' + i + '.jpg'))
        img = img[:, :, 1]

        for j in range(1, 11, 1):

            n, so, sk = computeJermanImages(img, j)
            Image.fromarray((n*255).astype(np.uint8)
                            ).save("jerman-n-"+str(j) + "-"+i+".jpg")

            Image.fromarray((so*255).astype(np.uint8)
                            ).save("jerman-so-"+str(j) + "-"+i+".jpg")

            norm = normalize(sk)
            Image.fromarray((norm*255).astype(np.uint8)
                            ).save("jerman-sk-"+str(j) + "-"+i+".jpg")


def scoresSingleFilter(imaIndex, plus1, plus3):
    # Computes Dice/MCC scores for filtered images with different σ and saves results in a file
    arr = np.zeros((2, 10))

    for indx, i in enumerate(imaIndex):
        scores = np.zeros((2, 10))
        for j in range(1, 11, 1):
            img = np.array(Image.open('results/single frangi/img ' +
                                      i+' f/frangi-sk-'+str(j)+'-'+i+'.jpg'))
            res = normalize(img)

            res[res <= 0.05] = 0
            res[res > 0.05] = 1

            truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
            truth = truth[:, :, 1]
            truth = normalize(truth)
            truth = imagePad(truth, res)

            truth[truth <= 0.9] = 0
            truth[truth > 0.9] = 1

            idx0 = np.nonzero(truth.ravel() == 1)[0]
            idxs = [idx0.min(), idx0.max()]
            out = np.column_stack(np.unravel_index(
                idxs, truth.shape)).flatten()

            truth = truth[out[0]-20:out[2]+20,
                          out[1]+plus1[indx]:out[3]+plus3[indx]]
            res = res[out[0]-20:out[2]+20, out[1] +
                      plus1[indx]:out[3]+plus3[indx]]

            mat = computeConfusionMatrix(
                truth.astype(np.int32), res.astype(np.int32))
            scores[0][j-1] = diceScore(mat)
            scores[1][j-1] = mccScore(mat)

    arr += scores

    arr = arr/len(imaIndex)
    with open("res-sk-f.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))


def frangiParameters(imaIndex):
    # Filters images with different parameters for Frangi filter and saves the filtered images
    aValues = [0.5, 0.8, 1.5, 1]
    bValues = [15, 25, 9, 10]

    for i in imaIndex:
        img = np.array(Image.open('PIMA/images/' + i + '.jpg'))
        img = img[:, :, 1]

        for indx, j in enumerate(aValues):
            n, so, sk = computeFrangiImages(img, a=j, b=bValues[indx])
            Image.fromarray((n*255).astype(np.uint8)
                            ).save("frangi-n-"+str(j) + "-"+str(bValues[indx])+"-"+i+".jpg")

            Image.fromarray((so*255).astype(np.uint8)
                            ).save("frangi-so-"+str(j) + "-"+str(bValues[indx])+"-"+i+".jpg")

            norm = normalize(sk)
            Image.fromarray((norm*255).astype(np.uint8)
                            ).save("frangi-sk-"+str(j) + "-"+str(bValues[indx])+"-"+i+".jpg")


def scoresFrangiParamters(imaIndex, plus1, plus3):
    # Computes Dice/MCC scores for Frangi filtered images with different parameters and saves results in a file
    aValues = [0.5, 0.8, 1.5, 1]
    bValues = [15, 25, 9, 10]
    arr = np.zeros((2, 4))

    for indx, i in enumerate(imaIndex):
        scores = np.zeros((2, 4))
        for indx2, j in enumerate(aValues):
            img = np.array(Image.open('results/single param frangi/img ' +
                                      i+' f/frangi-n-'+str(j)+"-"+str(bValues[indx2])+'-'+i+'.jpg'))

            res = normalize(img)

            res[res <= 0.05] = 0
            res[res > 0.05] = 1

            truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
            truth = truth[:, :, 1]
            truth = normalize(truth)
            truth = imagePad(truth, res)

            truth[truth <= 0.9] = 0
            truth[truth > 0.9] = 1

            idx0 = np.nonzero(truth.ravel() == 1)[0]
            idxs = [idx0.min(), idx0.max()]
            out = np.column_stack(np.unravel_index(
                idxs, truth.shape)).flatten()

            truth = truth[out[0]-20:out[2]+20,
                          out[1]+plus1[indx]:out[3]+plus3[indx]]
            res = res[out[0]-20:out[2]+20, out[1] +
                      plus1[indx]:out[3]+plus3[indx]]

            mat = computeConfusionMatrix(
                truth.astype(np.int32), res.astype(np.int32))
            scores[0][indx2] = diceScore(mat)
            scores[1][indx2] = mccScore(mat)

        arr += scores

    arr = arr/len(imaIndex)
    with open("param-n-f.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))


def jermanParameters(imaIndex):
    # Filters images with different parameters for Jerman filter and saves the filtered images
    params = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

    for i in imaIndex:
        img = np.array(Image.open('PIMA/images/' + i + '.jpg'))
        img = img[:, :, 1]

        for j in params:

            n, so, sk = computeJermanImages(img, t=j)
            Image.fromarray((n*255).astype(np.uint8)
                            ).save("jerman-n-"+str(j) + "-"+i+".jpg")

            Image.fromarray((so*255).astype(np.uint8)
                            ).save("jerman-so-"+str(j) + "-"+i+".jpg")

            norm = normalize(sk)
            Image.fromarray((norm*255).astype(np.uint8)
                            ).save("jerman-sk-"+str(j) + "-"+i+".jpg")


def scoresFrangiParamters(imaIndex, plus1, plus3):
    # Computes Dice/MCC scores for Jerman filtered images with different parameters and saves results in a file
    params = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    arr = np.zeros((2, 11))

    for indx, i in enumerate(imaIndex):
        scores = np.zeros((2, 11))
        for indx2, j in enumerate(params):
            img = np.array(Image.open('results/single param jerman/img ' +
                                      i+' j/jerman-n-'+str(j)+'-'+i+'.jpg'))
            res = normalize(img)

            res[res <= 0.2] = 0
            res[res > 0.2] = 1

            truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
            truth = truth[:, :, 1]
            truth = normalize(truth)
            truth = imagePad(truth, res)

            truth[truth <= 0.9] = 0
            truth[truth > 0.9] = 1

            idx0 = np.nonzero(truth.ravel() == 1)[0]
            idxs = [idx0.min(), idx0.max()]
            out = np.column_stack(np.unravel_index(
                idxs, truth.shape)).flatten()

            truth = truth[out[0]-20:out[2]+20,
                          out[1]+plus1[indx]:out[3]+plus3[indx]]
            res = res[out[0]-20:out[2]+20, out[1] +
                      plus1[indx]:out[3]+plus3[indx]]

            mat = computeConfusionMatrix(
                truth.astype(np.int32), res.astype(np.int32))
            scores[0][indx2] = diceScore(mat)
            scores[1][indx2] = mccScore(mat)

        arr += scores

    arr = arr/len(imaIndex)
    with open("param-n-j.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))


def filterMultiscale(imaIndex):
    # Filters images with multiscale framework and saves images
    for i in imaIndex:
        img = np.array(Image.open('PIMA/images/' + i + '.jpg'))
        img = img[:, :, 1]

        n, so, sk = multiscale(img, 2, "jerman")

        Image.fromarray((n*255).astype(np.uint8)
                        ).save("jerman-n-"+i+".jpg")

        Image.fromarray((so*255).astype(np.uint8)).save("jerman-so-"+i+".jpg")

        norm = normalize(sk)
        Image.fromarray((norm*255).astype(np.uint8)
                        ).save("jerman-sk-"+i+".jpg")


def scoresMultiscale(imaIndex, plus1, plus3):
    # Computes Dice/MCC scores for multiscale images and saves results in a file
    arr = np.zeros((2, 1))

    for indx, i in enumerate(imaIndex):
        scores = np.zeros((2, 1))
        img = np.array(Image.open(
            'results/multiscale/jerman sobel/jerman-so'+i+'.jpg'))
        res = normalize(img)

        res[res <= 0.9] = 0
        res[res > 0.9] = 1

        truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
        truth = truth[:, :, 1]
        truth = normalize(truth)
        truth = imagePad(truth, res)

        truth[truth <= 0.9] = 0
        truth[truth > 0.9] = 1

        idx0 = np.nonzero(truth.ravel() == 1)[0]
        idxs = [idx0.min(), idx0.max()]
        out = np.column_stack(np.unravel_index(idxs, truth.shape)).flatten()

        truth = truth[out[0]-20:out[2]+20,
                      out[1]+plus1[indx]:out[3]+plus3[indx]]
        res = res[out[0]-20:out[2]+20, out[1]+plus1[indx]:out[3]+plus3[indx]]

        mat = computeConfusionMatrix(
            truth.astype(np.int32), res.astype(np.int32))
        scores[0][0] = diceScore(mat)
        scores[1][0] = mccScore(mat)

        arr += scores

    arr = arr/len(imaIndex)
    with open("res-sk-f.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))


def filterRegion(imaIndex, plus1, plus3):
    # Filters images after cropping guide wire region and saves images
    for indx, i in enumerate(imaIndex):
        truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
        truth = truth[:, :, 1]
        truth = normalize(truth)
        img = np.array(Image.open('PIMA/images/' + i + '.jpg'))
        img = img[:, :, 1]
        idx0 = np.nonzero(truth.ravel() == 1)[0]
        idxs = [idx0.min(), idx0.max()]
        out = np.column_stack(np.unravel_index(idxs, truth.shape)).flatten()
        img = img[out[0]-20:out[2]+20, out[1]+plus1[indx]:out[3]+plus3[indx]]

        for j in range(4, 5, 1):

            n, so, sk = computeFrangiImages(img, j, j, j)
            Image.fromarray((n*255).astype(np.uint8)
                            ).save("jerman-area-n-"+str(j) + "-"+i+".jpg")

            Image.fromarray((so*255).astype(np.uint8)
                            ).save("jerman-area-so-"+str(j) + "-"+i+".jpg")

            norm = normalize(sk)
            Image.fromarray((norm*255).astype(np.uint8)
                            ).save("jerman-area-sk-"+str(j) + "-"+i+".jpg")


def scoresRegion(imaIndex, plus1, plus3):
    # Computes Dice/MCC scores for cropped filtered images and saves results in a file
    arr = np.zeros((2, 10))

    for indx, i in enumerate(imaIndex):
        scores = np.zeros((2, 10))
        for j in range(1, 11, 1):
            img = np.array(Image.open('results/single frangi area/img ' +
                                      i+' f/frangi-area-n-'+str(j)+'-'+i+'.jpg'))
            res = normalize(img)

            res[res > 0.7] = 0
            res[res <= 0.05] = 0
            res[res > 0.05] = 1

            truth = np.array(Image.open('PIMA/annotations/'+i+'.jpg'))
            truth = truth[:, :, 1]
            truth = normalize(truth)

            truth[truth <= 0.9] = 0
            truth[truth > 0.9] = 1

            idx0 = np.nonzero(truth.ravel() == 1)[0]
            idxs = [idx0.min(), idx0.max()]
            out = np.column_stack(np.unravel_index(
                idxs, truth.shape)).flatten()

            truth = truth[out[0]-19:out[2]+20,
                          out[1]+plus1[indx]:out[3]+plus3[indx]]

            truth = imagePad(truth, res)

            mat = computeConfusionMatrix(
                truth.astype(np.int32), res.astype(np.int32))
            scores[0][j-1] = diceScore(mat)
            scores[1][j-1] = mccScore(mat)
        arr += scores

    arr = arr/len(imaIndex)
    with open("res-area-n-f.txt", "w") as f:
        f.write("\n".join(" ".join(map(str, x)) for x in (arr)))
