import math
import numpy as np


def computeConfusionMatrix(groundTruth, image):
    # Computes Confusion Matrix
    # Input Images: Ground truth and filtered image, both segmented, binary and of same size

    matrix = np.zeros((2, 2))  # binary case
    for x in range(groundTruth.shape[0]):
        for y in range(groundTruth.shape[1]):
            matrix[groundTruth[x][y]][image[x][y]] += 1

    return matrix.astype(np.int32)


def diceScore(confMatrix):
    fp = int(confMatrix[1][0])
    fn = int(confMatrix[0][1])
    tp = int(confMatrix[1][1])

    dice = (2*tp)/(fp+fn+2*tp)
    return dice


def mccScore(confMatrix):
    tn = int(confMatrix[0][0])
    fp = int(confMatrix[1][0])
    fn = int(confMatrix[0][1])
    tp = int(confMatrix[1][1])

    mcc = ((tp*tn)-(fp*fn))/math.sqrt(((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn)))
    return mcc
