from singleFilter import computeFrangiImages, computeJermanImages
import numpy as np
from utils import imagePad


def multiscale(ima, dist, filter="frangi"):
    # Multicale Framework to compute filtered image
    # Input: Original image, Spacing for sigma values, Filter to be used
    # Output: Filtered Image

    fil = filSob = filSki = np.zeros(ima.shape)
    for j in range(2, 10, dist):
        if filter == "frangi":
            h, so, sk = computeFrangiImages(ima, j)
        elif filter == "jerman":
            h, so, sk = computeJermanImages(ima, j, t=1)

        # adapt size of fil, filSob, silSki to enlarged filtered image
        fil = imagePad(fil, h)
        filSob = imagePad(filSob, so)
        filSki = imagePad(filSki, sk)

        # combine previous image with newly filtered one
        fil = np.fmax(fil, h)
        filSob = np.fmax(filSob, so)
        filSki = np.fmax(filSki, sk)

    return fil, filSob, filSki
