
import numpy as np
from jerman_filter2D import jerman
import hessian
from frangi_filter2D import frangi
from eigenvalues import eigen
import time


def computeFrangiImages(image, sigmaSo=5, sigmaSk=6, sigmaD=3, a=0.5, b=15):
    start_time = time.time()
    # compute Frangi filtered Image using second order partial derivative Hessian function
    hes1, hes2, hes3 = hessian.hessian_with_derivative(image, sigmaD)
    Lambda1, Lambda2 = eigen(hes1, hes2, hes3)
    filtered = frangi(Lambda1, Lambda2, a, b)
    print("--- %s seconds derivative ---" % (time.time() - start_time))

    start_time = time.time()
    # compute Frangi filtered Image using Sobel filter Hessian funtion
    hes1Sobel, hes2Sobel, hes3Sobel = hessian.hessian_with_sobel(
        image, sigmaSo)
    Lambda1Sobel, Lambda2Sobel = eigen(hes1Sobel, hes2Sobel, hes3Sobel)
    filteredSobel = frangi(Lambda1Sobel, Lambda2Sobel, a, b)
    print("--- %s seconds sobel ---" % (time.time() - start_time))

    start_time = time.time()
    # compute Frangi filtered Image using Skimage Hessian function
    hes1Skimage, hes2Skimage, hes3Skimage = hessian.hessian_with_skimage(
        image, sigmaSk)
    Lambda1Skimage, Lambda2Skimage = eigen(
        hes1Skimage, hes2Skimage, hes3Skimage)
    filteredSkimage = frangi(Lambda1Skimage, Lambda2Skimage, a, b, True)
    print("--- %s seconds skimage ---" % (time.time() - start_time))

    return filtered, filteredSobel, filteredSkimage


def computeJermanImages(image, sigmaSo=4, sigmaSk=4, sigmaD=2, t=1.0):

    start_time = time.time()
    # compute Jerman filtered Image using second order partial derivative Hessian function
    hes1, hes2, hes3 = hessian.hessian_with_derivative(image, sigmaD)
    Lambda2 = eigen(hes1, hes2, hes3)[1]
    filtered = jerman(Lambda2, t)
    print("--- %s seconds derivative ---" % (time.time() - start_time))

    start_time = time.time()
    # compute Jerman filtered Image using Sobel filter Hessian funtion
    hes1Sobel, hes2Sobel, hes3Sobel = hessian.hessian_with_sobel(
        image, sigmaSo)
    Lambda2Sobel = eigen(hes1Sobel, hes2Sobel, hes3Sobel)[1]
    filteredSobel = jerman(Lambda2Sobel, t)
    print("--- %s seconds sobel ---" % (time.time() - start_time))

    start_time = time.time()
    # compute Jerman filtered Image using Skimage Hessian function
    hes1Skimage, hes2Skimage, hes3Skimage = hessian.hessian_with_skimage(
        image, sigmaSk)
    Lambda2Skimage = eigen(hes1Skimage, hes2Skimage, hes3Skimage)[1]
    filteredSkimage = jerman(Lambda2Skimage, t)
    print("--- %s seconds skimage ---" % (time.time() - start_time))

    return filtered, filteredSobel, filteredSkimage
