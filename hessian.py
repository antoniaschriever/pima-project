import numpy as np
from scipy.signal import convolve2d
from skimage import feature


def hessian_with_derivative(I, sigma=1):
    # Hessian matrix computed with the second derivative of the Gaussian
    # Input: Image I, standard deviation sigma
    # Output: Hessian Matrix Ixx, Ixy, Iyy

    n2 = np.round(3*sigma)
    x, y = np.meshgrid(np.arange(-n2, n2+1), np.arange(-n2, n2+1))

    Gxx = (-1 + x**2/sigma**2) * \
        ((np.exp(-(x**2+y**2)/(2*sigma**2)))/2*np.pi*sigma**4)
    Gyy = (-1 + y**2/sigma**2) * \
        ((np.exp(-(x**2+y**2)/(2*sigma**2)))/2*np.pi*sigma**4)
    Gxy = ((x*y)/(2*np.pi*sigma**6))*(np.exp(-(x**2+y**2)/(2*sigma**2)))

    Ixx = convolve2d(I, Gxx)
    Iyy = convolve2d(I, Gyy)
    Ixy = convolve2d(I, Gxy)

    return Ixx, Ixy, Iyy


def hessian_with_sobel(I, sigma=1):
    # Hessian matrix computed through two convolutions with Sobel filter
    # Input: Image I, standard deviation sigma
    # Output: Hessian Matrix Ixx, Ixy, Iyy

    n2 = np.round(3*sigma)
    x, y = np.meshgrid(np.arange(-n2, n2+1), np.arange(-n2, n2+1))
    kern = np.exp(-(x**2+y**2)/(2*sigma*sigma))
    kern = kern/kern.sum()
    img = convolve2d(I, kern)
    Sx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])

    Ixx = convolve2d(img, convolve2d(Sx, Sx))
    Iyy = convolve2d(img, convolve2d(Sx.T, Sx.T))
    Ixy = convolve2d(img, convolve2d(Sx, Sx.T))

    return Ixx, Ixy, Iyy


def hessian_with_skimage(I, sigma=1):
    # Hessian matrix computed by using the skimage function
    # Input: Image I, standard deviation sigma
    # Output: Hessian Matrix Ixx, Ixy, Iyy

    Ixx, Ixy, Iyy = feature.hessian_matrix(I, sigma)
    return Ixx, Ixy, Iyy
