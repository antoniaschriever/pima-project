import numpy as np


def eigen(Dxx, Dxy, Dyy):
    # Input: 2D Hessian Matrix
    # Output: 2 Eigenvalues, l1 being smaller than l2

    trace = Dxx + Dyy
    det = Dxx*Dyy - Dxy**2
    mid = np.sqrt(np.abs(trace**2-4*det))

    l1 = (trace - mid)/2
    l2 = (trace + mid)/2

    order = np.abs(l1) > np.abs(l2)
    l1[order], l2[order] = l2[order], l1[order]

    np.sort(l1, axis=0)
    np.sort(l2, axis=0)

    return l1, l2
